-- (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: user.org:user:pidiver:1.0
-- IP Revision: 13

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_iccfpga_pidiver_0_0 IS
  PORT (
    clk_fast : IN STD_LOGIC;
    clk_slow : IN STD_LOGIC;
    reset_slow : IN STD_LOGIC;
    reset_fast : IN STD_LOGIC;
    read_address : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    write_address : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    write_en : IN STD_LOGIC;
    read_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    write_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    led_overflow : OUT STD_LOGIC;
    led_running : OUT STD_LOGIC;
    led_found : OUT STD_LOGIC
  );
END design_iccfpga_pidiver_0_0;

ARCHITECTURE design_iccfpga_pidiver_0_0_arch OF design_iccfpga_pidiver_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_iccfpga_pidiver_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT pidiver IS
    GENERIC (
      VERSION_MAJOR : INTEGER;
      VERSION_MINOR : INTEGER;
      CALC_POW : INTEGER;
      HASH_LENGTH : INTEGER;
      STATE_LENGTH : INTEGER;
      NONCE_LENGTH : INTEGER;
      NUMBER_OF_ROUNDS : INTEGER;
      PARALLEL : INTEGER;
      INTERN_NONCE_LENGTH : INTEGER;
      BITS_MIN_WEIGHT_MAGINUTE_MAX : INTEGER;
      DATA_WIDTH : INTEGER;
      NONCE_OFFSET : INTEGER;
      CLOCK_SYNC : INTEGER;
      ENABLE_STOP : INTEGER;
      CALC_KECCAK384 : INTEGER;
      SHA3_384_HASH_SIZE : INTEGER;
      SHA3_384_DIGEST_LENGTH : INTEGER;
      SHA3_MAX_PERMUTATION_SIZE : INTEGER;
      SHA3_MAX_RATE_IN_QWORDS : INTEGER;
      SHA3_384_BLOCK_LENGTH : INTEGER;
      SHA3_384_BLOCK_LENGTH_BITS : INTEGER;
      SHA3_NUM_ROUNDS : INTEGER;
      CALC_TROIKA : INTEGER;
      COLUMNS : INTEGER;
      ROWS : INTEGER;
      SLICES : INTEGER;
      SLICESIZE : INTEGER;
      STATESIZE : INTEGER;
      NUM_SBOXES : INTEGER
    );
    PORT (
      clk_fast : IN STD_LOGIC;
      clk_slow : IN STD_LOGIC;
      reset_slow : IN STD_LOGIC;
      reset_fast : IN STD_LOGIC;
      read_address : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      write_address : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
      write_en : IN STD_LOGIC;
      read_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      write_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      led_overflow : OUT STD_LOGIC;
      led_running : OUT STD_LOGIC;
      led_found : OUT STD_LOGIC
    );
  END COMPONENT pidiver;
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF design_iccfpga_pidiver_0_0_arch: ARCHITECTURE IS "package_project";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset_fast: SIGNAL IS "XIL_INTERFACENAME reset_fast, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset_fast: SIGNAL IS "xilinx.com:signal:reset:1.0 reset_fast RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset_slow: SIGNAL IS "XIL_INTERFACENAME reset_slow, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset_slow: SIGNAL IS "xilinx.com:signal:reset:1.0 reset_slow RST";
BEGIN
  U0 : pidiver
    GENERIC MAP (
      VERSION_MAJOR => 1,
      VERSION_MINOR => 1,
      CALC_POW => 1,
      HASH_LENGTH => 243,
      STATE_LENGTH => 729,
      NONCE_LENGTH => 81,
      NUMBER_OF_ROUNDS => 81,
      PARALLEL => 6,
      INTERN_NONCE_LENGTH => 32,
      BITS_MIN_WEIGHT_MAGINUTE_MAX => 26,
      DATA_WIDTH => 9,
      NONCE_OFFSET => 162,
      CLOCK_SYNC => 1,
      ENABLE_STOP => 1,
      CALC_KECCAK384 => 1,
      SHA3_384_HASH_SIZE => 48,
      SHA3_384_DIGEST_LENGTH => 48,
      SHA3_MAX_PERMUTATION_SIZE => 25,
      SHA3_MAX_RATE_IN_QWORDS => 24,
      SHA3_384_BLOCK_LENGTH => 104,
      SHA3_384_BLOCK_LENGTH_BITS => 832,
      SHA3_NUM_ROUNDS => 24,
      CALC_TROIKA => 1,
      COLUMNS => 9,
      ROWS => 3,
      SLICES => 27,
      SLICESIZE => 27,
      STATESIZE => 729,
      NUM_SBOXES => 243
    )
    PORT MAP (
      clk_fast => clk_fast,
      clk_slow => clk_slow,
      reset_slow => reset_slow,
      reset_fast => reset_fast,
      read_address => read_address,
      write_address => write_address,
      write_en => write_en,
      read_data => read_data,
      write_data => write_data,
      led_overflow => led_overflow,
      led_running => led_running,
      led_found => led_found
    );
END design_iccfpga_pidiver_0_0_arch;
