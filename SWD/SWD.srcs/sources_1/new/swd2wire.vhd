-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- ecosystem: https://ecosystem.iota.org/projects/iota-crypto-core-fpga
--
-- donation-address: 
--   LLEYMHRKXWSPMGCMZFPKKTHSEMYJTNAZXSAYZGQUEXLXEEWPXUNWBFDWESOJVLHQHXOPQEYXGIRBYTLRWHMJAOSHUY
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
-- 
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SWD_Two_Wire is
    generic (
        LOCK : std_logic := '0'
    ); 
    Port ( M1_SWCLK : out STD_LOGIC;
           M1_SWDI : out STD_LOGIC;
           M1_SWDO : in STD_LOGIC;
           M1_SWDOEN : in STD_LOGIC;
           SWCLK : in STD_LOGIC;
           SWDIO : inout STD_LOGIC;
           lock_out : out std_logic);
end SWD_Two_Wire;

architecture Behavioral of SWD_Two_Wire is

begin
lock0: if LOCK = '0' generate 
    M1_SWCLK <= SWCLK;
    M1_SWDI <= SWDIO;
    SWDIO <= M1_SWDO when M1_SWDOEN='1' else 'Z';
end generate;

lock1: if LOCK = '1' generate
    M1_SWCLK <= '0';
    M1_SWDI <= '0';
    SWDIO <= 'Z';
end generate;

    lock_out <= LOCK;


end Behavioral;
